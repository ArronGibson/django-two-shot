from django import forms
from django.contrib.auth.forms import UserCreationForm


class LogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


class SignUpForm(UserCreationForm):
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
        label="Confirm Password"
    )