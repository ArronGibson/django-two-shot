from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .models import Receipt


@login_required(login_url='login')
def receipt_list_view(request):
    user = request.user
    receipts = Receipt.objects.filter(purchaser=user)
    context = {
        'receipts': receipts,
    }
    return render(request, 'receipts/receipt_list.html', context)